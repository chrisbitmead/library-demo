

Run the app with

  mvn spring-boot:run

  Access it in the browser at localhost:8080

Tested with Java 8 and Java 11

Tested with maven 3.6.1

I had some issues using Jetty, some incompatibility I was running into with Spring Boot.
That's why I left it using spring-boot's runner.
