package com.chrisb.library.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author Chris Bitmead
 * @Date 18/5/2019
 */
@Entity
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String title;
    private String author;
    private String isbn;

    @ManyToMany
    @JoinTable(name = "person_book", joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "person_id"))
    private Set<Person> borrowers = new HashSet<>();

    //Constructors from the empty to complete
    public Book() {
    }

    public Book(String title, String author, String isbn) {
        this.title = title;
        this.author = author;
        this.isbn = isbn;
    }

    //Getters and Setters
    public Long getId() { return id; }

    public void setId(Long id) { this.id = id; }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title;}

    public String getAuthor() { return author; }

    public void setAuthor(String author) { this.author = author;}

    public String getIsbn() { return isbn; }

    public void setIsbn(String isbn) { this.isbn = isbn; }

    public Set<Person> getBorrowers() { return borrowers; }

    public void setBorrowers(Set<Person> borrowers) { this.borrowers = borrowers; }

    // ToEquals and HashCode methods
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;

        Book book = (Book) o;

        return id != null ? id.equals(book.id) : book.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    // ToString Method
    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", isbn='" + isbn + '\'' +
                ", borrowers=" + borrowers +
                '}';
    }
}
