package com.chrisb.library.controllers;

import com.chrisb.library.repositories.BookRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @Author Chris Bitmead
 * @Date 18/5/2019
 */
@Controller
public class BooksController {

    private BookRepository bookRepository;

    public BooksController(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @RequestMapping("/books")
    public String getBooks(Model model)  {

        model.addAttribute("books", bookRepository.findAll());
        return "books";
    }

}
