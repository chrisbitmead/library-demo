package com.chrisb.library.controllers;

import com.chrisb.library.repositories.PersonRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @Author Chris Bitmead
 * @Date 18/5/2019
 */
@Controller
public class PeopleController {

    private PersonRepository personRepository;

    public PeopleController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @RequestMapping("/people")
    public String getPeople(Model model) {
        model.addAttribute("people", personRepository.findAll());
        return "people";
    }
}
