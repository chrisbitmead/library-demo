package com.chrisb.library.controllers;

import com.chrisb.library.repositories.PersonRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.chrisb.library.model.Person;

/**
 * @Author Chris Bitmead
 * @Date 18/5/2019
 */

@Controller
public class PersonController {

    private PersonRepository personRepository;

    public PersonController(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @RequestMapping("/person")
    public String getPeople(Model model, @RequestParam("id") Long id) {
        Person person = personRepository.findById(id).get();
        System.out.println("Person: " + person.getFirstName());
        model.addAttribute("person", person);

        return "person";
    }

    @GetMapping(value = "/booksForPerson")
    public String booksForPerson(Model model, @RequestParam("id") Long id) {
        Person person = personRepository.findById(id).get();
        model.addAttribute("books", person.getBooks());
        return "person :: div_content";
    }
}
