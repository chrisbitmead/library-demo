package com.chrisb.library.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @Author Chris Bitmead
 * @Date 18/5/2019
 */
@Controller
public class WebController {
    @GetMapping(value="/")
    public String homepage(){
        return "index";
    }
}