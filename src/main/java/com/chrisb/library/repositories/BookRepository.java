package com.chrisb.library.repositories;

import com.chrisb.library.model.Book;
import org.springframework.data.repository.CrudRepository;

/**
 * @Author Chris Bitmead
 * @Date 18/5/2019
 */
public interface BookRepository extends CrudRepository<Book, Long> {

}
