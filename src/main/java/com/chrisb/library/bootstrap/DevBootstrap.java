package com.chrisb.library.bootstrap;

import com.chrisb.library.model.Book;
import com.chrisb.library.repositories.BookRepository;
import com.chrisb.library.repositories.PersonRepository;
import com.chrisb.library.model.Person;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * @Author Chris Bitmead
 * @Date 18/5/2019
 */
@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private PersonRepository personRepository;
    private BookRepository bookRepository;

    public DevBootstrap(PersonRepository personRepository, BookRepository bookRepository) {
        this.personRepository = personRepository;
        this.bookRepository = bookRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData() {
        //Laura
        Person chris = new Person("Chris", "Smith", "6211111111", "chris@gmail.com");
        Book mockingbird = new Book("To Kill a Mockingbird", "Harper Lee", "978-0060935467");
        Book pride = new Book("Pride and Prejudice", "Jane Austen", "978-1503290563");
        chris.getBooks().add(mockingbird);
        chris.getBooks().add(pride);
        mockingbird.getBorrowers().add(chris);
        pride.getBorrowers().add(chris);
        personRepository.save(chris);
        bookRepository.save(mockingbird);
        bookRepository.save(pride);


        Person melissa = new Person("Melissa", "Sanders", "6222222222", "melissa@gmail.com");
        Book snowyRiver = new Book("The Man from Snowy River and Other Verses", "A. B. Paterson", "978-1451015928");
        Book catcher = new Book("The Catcher in the Rye", "J.D. Salinger", "978-7543321724");
        melissa.getBooks().add(snowyRiver);
        melissa.getBooks().add(catcher);
        snowyRiver.getBorrowers().add(melissa);
        catcher.getBorrowers().add(melissa);
        personRepository.save(melissa);
        bookRepository.save(snowyRiver);
        bookRepository.save(catcher);


        Person bill = new Person("Bill", "Bloggs", "6233333333", "bill@gmail.com");
        personRepository.save(bill);
        mockingbird.getBorrowers().add(bill);
        bookRepository.save(mockingbird);
    }
}
